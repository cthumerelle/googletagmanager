# README #

This module brings Google Tag Manager to your prestashop setup.
It creates a dataLayer with informations about the user, his cart, the context or orders. You can then use simple GTM macros to retrieve the data into GTM.

##Setup##
In order to use this module you need to setup a GTM account [here](http://www.google.com/tagmanager/).

To setup your account you will need a container ID (that you get when your create a container in GTM).

You can change de dataLayer variable name if you need to. This would be an advanced setting.

##GTM configuration##
You will need a few basic macros to get running.

 - context
 - page
 - product
 - category
 - search
 - cart
 - order
 - user

Those macros are `data Layer Variable`s and use the 2nd version of the DataLayer.

You **will** need to create more macro for a functional GTM setup. For instance you might want to setup a tag to fire only on product pages.

You will need to create a macro like this one :

| Attribute                 | value
| ------------------------: | :------------------
| `Macro Name`              | page.name
| `Macro Type`              | Data Layer Variable
| `Data Layer Variable Name`| page.controller
| `Data Layer Version`      | Version 2

And then use this macro in a `Rule`. For instance :

Condition :

    {{page.name}} equals index
    
Here is an example of a Tag for Criteo integration on the a product page :

    <!-- Tag Criteo Fiche Produit -->
    <script type="text/javascript">
    // session ID randomized and stored in a cookie - or user ID if user is logged in
    if(!{{user}}.logged){
      var re = new RegExp("critsessid=([^;]+)");
      var sessionid = (re.exec(document.cookie))?re.exec(document.cookie)[1]:false;
      if(!sessionid){
        sessionid = "UV"+Math.floor((Math.random()*10000000000)+1);;
        var d = new Date();
        d.setTime(d.getTime()+(365*24*60*60*1000));
        document.cookie="critsessid="+sessionid+"; expires="+d.toGMTString();
      }
    } else {
        var sessionid={{user}}.id;
    }
    </script>
    <script type="text/javascript" src="http://static.criteo.net/js/ld/ld.js" async="true"></script>
    <script type="text/javascript">
    window.criteo_q = window.criteo_q || [];
    window.criteo_q.push(  
    		{ event: "setAccount", account: {{Criteo - account}}}, // you would create a macro to store you criteo ID
    		{ event: "setCustomerId", id: sessionid },
    		{ event: "setSiteType", type: "d" },
    		{ event: "viewItem", item: {{product}}.id } // You need to send the current product ID.
    );
    </script>

This is a example if you needed to pull the first 3 product ids of a category page :

    var products = {{category}}.list.prods;
    var viewList = new Array();
    var prodId;
    if(products != undefined){
    	for(var i = 0; i < 3; i++) {
    		if(i < products.length){
    			prodId = products[i].id;
    			viewList.push(prodId);
    		}
    	}
    } 
