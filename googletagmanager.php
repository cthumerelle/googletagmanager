<?php
include_once('charlymodule/moduleHandler.php');

class GoogleTagManager extends CharlyModule {

	static $page = false;
  static $dataLayer = array();

  public function __construct() {
    $this->name = 'googletagmanager';
    $this->displayName = 'Google Tag Manager';
    $this->description = $this->l('Deploy tag seamlessly on your website');
    $this->tab = 'AdminParentStats';
    $this->version = '0.1';
    $this->paymentModules = array();
    $this->submitName = 'submitGTM';
    $this->author = 'BAMF Consulting';
		$this->need_instance = 0;
		$this->secure_key = Tools::encrypt($this->name);
    $this->prefix = 'GTM_';
    $this->hooks = array('displayHeader','displayOrderConfirmation','displayFooterProduct','displayFooter');


    parent::__construct();

    if ( !$this->confGet('CONTENER_ID')){ $this->warning = $this->l('Please configure your Google Tag Manager Account');}

    $this->settings = array(
      'CONTENER_ID' => array('type'=>'text','label'=> $this->l('Contener ID'),'infos'=>$this->l('your contener ID (e.g. GTM-XXXXXX)')),
      'DATALAYER_NAME' => array('type'=>'text','label'=> $this->l('nom de la variable contenant le dataLayer'),'infos'=>$this->l('par défaut "dataLayer"'),'default'=>'dataLayer'),
    );
  }
  
  public function hookDisplayHeader($params) {
    //initialisation 
    //print_r($this->context);
    static::$dataLayer = array(
      'context'=>new stdClass(),
      'page'=>new stdClass(),
      'product'=>new stdClass(),
      'category'=>new stdClass(),
      'list'=> new StdClass(),
      'search'=>new stdClass(),
      'cart'=>new stdClass(),
      'order'=>new stdClass(),
      'user'=>new stdClass()
    );
    //print_r($this->context);
    static::$dataLayer['page']->controller = $controller = isset($this->context->controller->php_self)?$this->context->controller->php_self:"";
    static::$dataLayer['user']->logged = $this->context->customer->logged?true:false;
    if(static::$dataLayer['user']->logged){
      static::$dataLayer['user']->lastname = $this->context->customer->lastname;
      static::$dataLayer['user']->firstname = $this->context->customer->firstname;
      static::$dataLayer['user']->birthday = $this->context->customer->birthday;
      static::$dataLayer['user']->email = $this->context->customer->email;
      static::$dataLayer['user']->newsletter = $this->context->customer->newsletter;
      static::$dataLayer['user']->date_add = $this->context->customer->date_add;
      static::$dataLayer['user']->id = $this->context->customer->id;
    }
    //Tools::getValue('step')

    //echo $controller;
  }
  
  public function hookDisplayFooter() {

    //Let's get data on current page. That's in current controller.
    $controller = isset($this->context->controller->php_self)?$this->context->controller->php_self:"";
    
    //if we're on a product page
    if($controller =='product' && $P = $this->context->controller->getProduct()){
      static::$dataLayer['page']->name = "product page";
      $p = new stdClass();
      $p->id = $P->id;
      $p->name = $P->name;
      $p->description = $P->description;
      $p->quantity = $P->quantity;
      $p->price_ht = (float)$P->price;
      $p->price_ttc = (float)Product::getPriceStatic($P->id,true);
      $p->on_sale = (bool)$P->on_sale;
      $p->ecotax = (float)$P->ecotax;
      $p->reference = $P->reference;
      $p->supplier_reference = $P->supplier_reference;
      $p->brand = Manufacturer::getNameById((int)$P->id_manufacturer);
      $p->width = (float)$P->width;
      $p->height = (float)$P->height;
      $p->depth = (float)$P->depth;
      $p->weight = (float)$P->weight;
      $p->ean13 = $P->ean13;
      $p->link_rewrite = $P->link_rewrite;
      $p->new = (bool)$P->new;
      $p->condition = $P->condition;
      $this->addCategoryInfos($p,$P->id_category_default);
      
      static::$dataLayer['product'] = $p;
    }
    
    //if we're in a category list
    if($controller == "category" && $C = $this->context->controller->getCategory()){
      static::$dataLayer['page']->name = "category page";
      
      static::$dataLayer['category']->id = $C->id;
      static::$dataLayer['category']->name = $C->name;
      $parent_cates = $C->getParentsCategories();
      static::$dataLayer['category']->breadcrumb_names = array();
      static::$dataLayer['category']->breadcrumb_ids = array();
      array_splice($parent_cates,-1,1); // on enlève la dernière catégorie : root
      $parent_cates = array_reverse($parent_cates);
      foreach($parent_cates as $cat){
        static::$dataLayer['category']->breadcrumb_names[]=$cat['name'];
        static::$dataLayer['category']->breadcrumb_ids[]=$cat['id_category'];
      }
      $prods =  $C->getProducts($this->context->language->id, $this->context->controller->p, $this->context->controller->n, $this->context->controller->orderBy, $this->context->controller->orderWay);
      static::$dataLayer['list']->prods = array();
      $pos = 1;
      foreach($prods as $P){
        $p = new stdClass();
        $p->id = $P['id_product'];
        $p->name = $P['name'];
        $p->description = $P['description'];
        $p->quantity = $P['quantity'];
        $p->price_ht = (float)$P['price'];
        $p->price_ttc = (float)Product::getPriceStatic($P['id_product'],true);
        $p->on_sale = (bool)$P['on_sale'];
        $p->ecotax = (float)$P['ecotax'];
        $p->reference = $P['reference'];
        $p->supplier_reference = $P['supplier_reference'];
        $p->brand = Manufacturer::getNameById((int)$P['id_manufacturer']);
        $p->width = (float)$P['width'];
        $p->height = (float)$P['height'];
        $p->depth = (float)$P['depth'];
        $p->weight = (float)$P['weight'];
        $p->ean13 = $P['ean13'];
        $p->link_rewrite = $P['link_rewrite'];
        $p->new = (bool)$P['new'];
        $p->condition = $P['condition'];
        $this->addCategoryInfos($p,$P['id_category_default']);
        $p->position = $pos++;
        static::$dataLayer['list']->prods[] = $p;
      }
      static::$dataLayer['list']->p = $this->context->controller->p;
      static::$dataLayer['list']->n = $this->context->controller->n;
      static::$dataLayer['list']->orderBy = $this->context->controller->orderBy;
      static::$dataLayer['list']->orderWay = $this->context->controller->orderWay;
    }
    
    //if we have a cart (and we probably do)
    if($C = $this->context->cart){
      $c = new stdClass();
      $c->id = $C->id;
      $c->date_add = $C->date_add;
      static::$dataLayer['cart'] = $c;

      $products = $C->getProducts();
      static::$dataLayer['cart']->products = array();
      foreach($products as $P){
        $p = new stdClass();
        $p->id = $P['id_product'];
        $p->id_product_attribute = $P['id_product_attribute'];
        $p->name = $P['name'];
        $p->description = $P['description_short'];
        $p->quantity = $P['quantity'];
        $p->price_ht = (float)$P['price'];
        $p->price_ttc = (float)Product::getPriceStatic($P['id_product'],true,$P['id_product_attribute']);
        $p->on_sale = (bool)$P['on_sale'];
        $p->ecotax = (float)$P['ecotax'];
        $p->reference = $P['reference'];
        $p->supplier_reference = $P['supplier_reference'];
        $p->brand = Manufacturer::getNameById((int)$P['id_manufacturer']);
        $p->width = (float)$P['width'];
        $p->height = (float)$P['height'];
        $p->depth = (float)$P['depth'];
        $p->weight = (float)$P['weight'];
        $p->ean13 = $P['ean13'];
        $p->link_rewrite = $P['link_rewrite'];
        $this->addCategoryInfos($p,$P['id_category_default']);
        static::$dataLayer['cart']->products[] = $p;
      }
      
    }

    //CHECKOUT step identification

    $page = false;
    $checkout_step = false;
    $checkout_option = false;

    // cart : not identified
    if($controller == "order" && !$this->context->customer->logged){
      $page = "cart/not_loggued";
      $checkout_step = 1;
      $checkout_option = "not loggued in";
    }

    // checkout : identification
    if($controller == "authentication" && Tools::getValue('back') !== "my-account" && Tools::getValue('display_guest_checkout') !== false){
      $page = "checkout/identification";
      $checkout_step = 2;
    }

    // checkout : create an account
    if($controller == "authentication" && Tools::getValue('back') !== "my-account" && Tools::getValue('display_guest_checkout') === false){
      $page = "checkout/create_account";
      $checkout_step = 3;
    }

    // cart : identified
    if($controller == "order" && $this->context->customer->logged){
      $page = "cart/loggued";
      $checkout_step = 4;
      $checkout_option = "loggued in";
    }

    // checkout : adresses
    if($controller == "address" && strpos(Tools::getValue('back'),'order.php') !== false){
      $page = "checkout/step1";
      $checkout_step = 5;
    }

    // checkout : adresses
    if($controller == "order" && Tools::getValue('step') == 1){
      $page = "checkout/addresses";
      $checkout_step = 6;
      $checkout_option = false;
    }

    // checkout : carrier
    if($controller == "order" && Tools::getValue('step') == 2){
      $page = "checkout/carriers";
      $checkout_step = 7;
      $checkout_option = false;
    }

    // checkout : choose payment method
    if($controller == "order" && Tools::getValue('step') == 3){
      $page = "checkout/payments";
      $checkout_step = 8;
      $checkout_option = false;
    }

    // checkout : order confirmation
    if($controller == "order-confirmation")
    {
      $page = "checkout/order-confirmation";
      $checkout_step = 9;
      if($id_module = Tools::getValue('id_module'))
      {
        if($module = Module::getInstanceById($id_module))
        {
          $page.='/'.$module->name.'/'.Tools::getValue('id_order');
          $checkout_option = $module->name;
        }
      }
    }

    static::$dataLayer['page']->name = $page;
    static::$dataLayer['page']->checkout_step = $checkout_step;
    static::$dataLayer['page']->checkout_option = $checkout_option;
    
    $page = static::$page;
    $output  = '<!-- Google Tag Manager -->
    <script>';
    $output .= 'var dataLayer = [];';
    foreach(static::$dataLayer as $macro => $item){
    if(count((array)$item)){
      $output .='dataLayer.push({"'.$macro.'":'.json_encode($item).'});';
    }
    }
    $output .= '</script>';
    $output .= '
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id='.$this->confGet('CONTENER_ID').'"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
    new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
    \'//www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,\'script\',\'dataLayer\',\''.$this->confGet('CONTENER_ID').'\');</script>
    <!-- End Google Tag Manager -->
    ';
    return $output;

  }

  public function hookDisplayOrderConfirmation($params) {
    $lang = $params['cookie']->id_lang;
    $order = $params['objOrder'];
    if (Validate::isLoadedObject($order)) {
      $address = new Address(intval($order->id_address_delivery));
      $conversion_rate = 1;
      if ($order->id_currency != Configuration::get('PS_CURRENCY_DEFAULT')) {
          $currency = new Currency(intval($order->id_currency));
          $conversion_rate = floatval($currency->conversion_rate);
      }
      static::$dataLayer['order']->id = intval($order->id);
      static::$dataLayer['order']->reference = $order->reference;
      static::$dataLayer['order']->id_cart = intval($order->id_cart);
      static::$dataLayer['order']->payment = $order->payment;
      static::$dataLayer['order']->total_ht = (float)$order->total_paid_tax_excl;
      static::$dataLayer['order']->total_ttc = (float)$order->total_paid_tax_incl;
      static::$dataLayer['order']->total_products_ht = (float)$order->total_products;
      static::$dataLayer['order']->total_products_ttc = (float)$order->total_products_wt;
      static::$dataLayer['order']->total_shipping_ht = (float)$order->total_shipping_tax_excl;
      static::$dataLayer['order']->total_shipping_ttc = (float)$order->total_shipping_tax_incl;
      static::$dataLayer['order']->total_discount_ht = (float)$order->total_discounts_tax_excl;
      static::$dataLayer['order']->total_discount_ttc = (float)$order->total_discounts_tax_incl;
      
      $products = $order->getProducts();
      
      static::$dataLayer['order']->products = array();
      foreach ($products AS $product) {
        $product_id = $product['product_id'];
        $P = new Product($product_id);
        $C = new Category((int) $P->id_category_default, $lang);
        $p = new stdClass();
        
        $p->id = $P->id;
        $p->name = $P->name[$lang];
        //$p->description = $P->description[$lang];
        $p->quantity = intval($product['product_quantity']);
        $p->price_ht = (float)$P->price;
        $p->price_ttc = (float)Product::getPriceStatic($P->id,true);
        $p->on_sale = (bool)$P->on_sale;
        $p->ecotax = (float)$P->ecotax;
        $p->supplier_reference = $P->supplier_reference;
        $p->width = (float)$P->width;
        $p->height = (float)$P->height;
        $p->depth = (float)$P->depth;
        $p->weight = (float)$P->weight;
        $p->ean13 = $P->ean13;
        $p->link_rewrite = $P->link_rewrite[$lang];
        $p->brand = Manufacturer::getNameById((int)$P->id_manufacturer);
        //$p->new = (bool)$P->new;
        $p->condition = $P->condition;
        $this->addCategoryInfos($p,$P->id_category_default);
        
        static::$dataLayer['order']->products[]= $p;
      }
    }
  }

  public function hookDisplayFooterProduct($params)
  {
    // detect ajax cart and fires page when addind a product to the cart
  }

  public function addCategoryInfos(&$p,$id_category){
    $C = new Category($id_category);
    $parent_cates = $C->getParentsCategories();
    $p->breadcrumb_names = array();
    $p->breadcrumb_ids = array();
    array_splice($parent_cates,-1,1); // on enlève la dernière catégorie : root
    $parent_cates = array_reverse($parent_cates);
    foreach($parent_cates as $cat){
      $p->breadcrumb_names[]=$cat['name'];
      $p->breadcrumb_ids[]=$cat['id_category'];
    }
    $p->category_name = implode('/', $p->breadcrumb_names);
    $p->category_id = $id_category;
  }

}
